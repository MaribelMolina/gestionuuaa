import { LitElement, html, css } from 'lit-element';

class GuardiaHeader extends LitElement {
    static get styles () {
        return css`
            .bg-bbva {
                background-color: #004481 !important;
                color: #CCC;
            }
        `
    }
    render() {
        return html`
         <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">      
        <header>
            <div class="navbar navbar-dark bg-bbva shadow-sm">
             <div class="container d-flex justify-content-between">
                <a href="#" class="navbar-brand d-flex align-items-center">
                    <strong>Gestión de UUAAs de Valores</strong>
                </a>
             </div>
            </div>
        </header>
        `;
    }
}

customElements.define('guardia-header', GuardiaHeader)