//import { LitElement, html} from 'lit-element';
import { LitElement, html, css} from 'lit-element';
import '../guardia-ficha-listado/guardia-ficha-listado.js';
import '../guardia-user-form/guardia-user-form.js';
import '../guardia-guardia-form/guardia-guardia-form.js';

class GuardiaMain  extends LitElement {

    static get properties(){
        return {
            guardiasUUAA: {type: Array},
            fechasFilter: {type: Object},
            showUserForm: {type: Boolean},
            showUUAAForm: {type: Boolean},
            showGuardiaForm: {type: Boolean}
        };
    }

    constructor(){
        super();    
        this.guardiasUUAA=[];
        this.showUserForm=false;
        this.showUUAAForm=false;
        this.showGuardiaForm=false;
    }

render(){ 
        return html`
         <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">      
            <h2>Responsables</h2>
            <div class="row" id="guardiaList">
                <div class="row row-cols-12 text-center ">
                    ${this.guardiasUUAA.filter(
                        guardia => (guardia.fechaGuardia >= this.fechasFilter.fechaInicio
                        && guardia.fechaGuardia <=this.fechasFilter.fechaFin)).map(
                          guardia =>  html`
                            <guardia-ficha-listado
                                 .user="${guardia.user}" 
                                 nameUUAA="${guardia.nameUUAA}"
                                 fechaGuardia="${guardia.fechaGuardia}"
        
                            >
                            </guardia-ficha-listado>`
                    )}
                </div>
            </div>
            <div>
                <guardia-user-form id="userForm" class="d-none" @user-form-close="${this.userFormClose}"
                                                                @user-form-store="${this.userFormStore}">
                </guardia-user-form>
            </div>
            <div>
            <guardia-guardia-form id="guardiaForm" class="d-none" 
            @guardia-form-close="${this.guardiaFormClose}"
            @guardia-form-store="${this.guardiaFormStore}">
            </guardia-user-form>
        </div>
    
        `;
    }




    updated(changedProperties){
        console.log("updated");

        if(changedProperties.has("showUserForm")){
            console.log("Ha cambiado el valor de la propiedad showUserForm en persona main")
        
            if(this.showUserForm === true){
                this.showUserFormData();
            }else{
                this.showGuardiaList();
            }
        }

        if(changedProperties.has("showGuardiaForm")){
            console.log("Ha cambiado el valor de la propiedad showGuardiaForm en persona main")
        
            if(this.showGuardiaForm === true){
                this.showGuardiaFormData();
            }else{
                this.showGuardiaList();
            }
        }
    }

    userFormClose(e){
        console.log("personFormClose");
        console.log("Se ha cerrado el formulario de el user");
        this.showUserForm =false;
    }

    userFormStore(e){
        console.log("userFormStore");
        console.log("Se va a almacenar un user");
        console.log(e.detail)
      
        console.log("Usuario almacenado");
        this.showUserForm=false;

    }

    guardiaFormClose(e){
        console.log("guardiaFormClose");
        console.log("Se ha cerrado el formulario de el guardia");
        this.showGuardiaForm =false;
    }

    guardiaFormStore(e){
        console.log("guardiaFormStore");
        console.log("Se va a almacenar un guardia");
        console.log(e.detail)

     
        this.dispatchEvent(new CustomEvent("guardia-store",{
            detail: {
                codUuaa: e.detail.codUuaa,
                fechaGuardia: e.detail.fechaGuardia,
                user: {
                    codUser: e.detail.user.codUser
                }
            }
        }));

        console.log("Guardia almacenado");
        this.showGuardiaForm=false;


    }

    //Mostrar la lista de Guardias
    showGuardiaList(){
        console.log("showGuardiaList");
        this.shadowRoot.getElementById("guardiaList").classList.remove("d-none");
        this.shadowRoot.getElementById("userForm").classList.add("d-none");
        this.shadowRoot.getElementById("guardiaForm").classList.add("d-none");
    }

    //Mostrar el formulario de Usuario
    showUserFormData(){
        console.log("showUserFormData");
        this.shadowRoot.getElementById("guardiaList").classList.add("d-none");
        this.shadowRoot.getElementById("guardiaForm").classList.add("d-none");
        this.shadowRoot.getElementById("userForm").classList.remove("d-none");
    }

    //Mostrar el formulario de Guardia
    showGuardiaFormData(){
        console.log("showGuardiaFormData");
        this.shadowRoot.getElementById("guardiaList").classList.add("d-none");
        this.shadowRoot.getElementById("userForm").classList.add("d-none");
        this.shadowRoot.getElementById("guardiaForm").classList.remove("d-none");
    }
  
}

customElements.define('guardia-main', GuardiaMain)