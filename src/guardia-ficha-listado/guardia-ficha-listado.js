import { LitElement, html,css} from 'lit-element';

class GuardiaFichaListado  extends LitElement {

    //Definimos la propiedad 
    static get properties(){
        return{
            user: {type: Object},
            nameUUAA: {type: String},
            fechaGuardia: {type: String}
        }
    }

    static get styles () {
        return css`
            .bg-bbva {
                background-color: #004481 !important;
                color: #CCC;
            }
        `
    }

    constructor(){
        super();
        this.user={};
    }

    render(){ 
        return html`
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">      
            <div class="card h-100 w-200 ml-5 badge-info">
                <div class="card header">
                    <h3 class="card-title bg-bbva" >${this.nameUUAA}</h3>
                </div>
                <div class="card-body">
                    <p class="card-text" >${this.user.nameUser} </p>
                    <p class="card-text" >${this.user.telUser} </p>
                    <p class="card-text" >${this.fechaGuardia} </p>
                </div>      
              
            </div>
        `;
    }


    moreInfo(e){
        console.log ("moreInfo");
        console.log("Se solicita información de la UUAA " + this.nameUUAA);

        this.dispatchEvent( 
            new CustomEvent("info-uuaa",{
                detail: {
                    name: this.nameUUAA
                }
            })
        )
    }

}


customElements.define('guardia-ficha-listado', GuardiaFichaListado)