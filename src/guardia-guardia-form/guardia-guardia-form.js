import { LitElement, html} from 'lit-element';


class GuardiaGuardiaForm  extends LitElement {


    static get properties(){
        return {
            user: {type: Object}
        }
    }

    constructor(){
        super();
           
        this.resetFormData();

    }
    render(){ 
        return html`
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">      
        <div class="row">
            <form>
                <div class="form-group">
                    <label>Seleccione UUAA</label>
                    <input type="text" required class="form-control" placeholder="Código UUAA" maxlength="4"
                    @input="${this.updateCodUuaa}"
                    .value="${this.codUuaa}">
                </div>
                <div class="form-group">
                    <label>Código de usuario</label>
                    <input type="text" required class="form-control" placeholder="Código usuario" maxlength="8"
                    @input="${this.updateCodUser}"
                    .value="${this.user.codUser}">
                </div>
                <div class="form-group">
                    <label>Fecha de guardia</label>
                    <input type="date" required id="userFormName"/ class="form-control" placeholder="Fecha de guardia"
                        maxlength="10"
                    @input="${this.updateFechaGuardia}"
                    .value="${this.fechaGuardia}"
                    />
                </div>
                <button class="btn btn-primary" @click="${this.goBack}"><strog>Atrás</strong></button>
                <button class="btn btn-success" @click="${this.storeGuardia}"><strog>Guardar</strong></button>     
            </form>
         </div>
        
        `;
    }

    updateCodUuaa(e){
        console.log("Actualizando la propiedad codUuaa con el valor" + e.target.value);
        this.codUuaa = e.target.value;
    }

    updateCodUser(e){
        console.log("Actualizando la propiedad user con el valor" + e.target.value);
        this.user.codUser = e.target.value;
    }

    updateFechaGuardia(e){
        console.log("Actualizando la propiedad fechaGuardia con el valor" + e.target.value);
        this.fechaGuardia = e.target.value;
    }


    resetFormData() {
        this.user={};
        this.codUuaa= "";
        this.user.codUser= "";
        this.fechaGuardia= "";
    };

    goBack(e) {

        console.log("goBack");
        e.preventDefault();
        this.dispatchEvent(new CustomEvent("guardia-form-close",{}));
        this.resetFormData();
    }
    
    storeGuardia(e){
        console.log("StoreGuardia");
        e.preventDefault();
     
        this.dispatchEvent(new CustomEvent("guardia-form-store",{
            detail: {
                codUuaa: this.codUuaa,
                fechaGuardia: this.fechaGuardia,
                user: {
                    codUser: this.user.codUser
                }
            }
        })        );
        this.resetFormData();

    }

}

customElements.define('guardia-guardia-form', GuardiaGuardiaForm)