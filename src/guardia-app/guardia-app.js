import { LitElement, html} from 'lit-element';
import '../guardia-header/guardia-header.js';
import '../guardia-main/guardia-main.js';
import '../guardia-footer/guardia-footer.js';
import '../guardia-api/guardia-api.js';
import '../guardia-sidebar/guardia-sidebar.js';

class GuardiaApp  extends LitElement {

    //Creamos propiedad desde donde voy a recoger la información
  /*  static get properties(){
        return {
   
        };
    }*/

    constructor (){
        super();
    }

    render(){ 
        return html`
            <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">     
            <guardia-header></guardia-header>
            <div class="row">
                <guardia-sidebar class="col-2" 
                @updated-fechas="${this.updateFechas}"
                @new-user="${this.newUser}"
                @new-guardia="${this.newGuardia}"
                @new-UUAA="${this.newUUAA}"
                >
                </guardia-sidebar>
                <guardia-main class="col-10" 
                @guardia-store="${this.guardiaStore}"
                >
                 </guardia-main>
               
            </div>
            <guardia-footer></guardia-footer>
         <guardia-api  @guardia-api="${this.updateGuardia}"> </guardia-api>  
        `;
    }

    newUser(e)
    {
        console.log("Añadimos un nuevo usuario")
        this.shadowRoot.querySelector("guardia-main").showUserForm=true;

    }

    newUUAA(e)
    {
        console.log("Añadimos una nueva UUAA")
        this.shadowRoot.querySelector("guardia-main").showUUAAForm=true;

    }

    newGuardia(e)
    {
        console.log("Añadimos una nueva guardia")
        this.shadowRoot.querySelector("guardia-main").showGuardiaForm=true;

    }

    updateGuardia(e){
        console.log("Capturamos el evento de la api")
        console.log(e.detail);
        this.shadowRoot.querySelector('guardia-main').guardiasUUAA = e.detail.guardiasUUAA;
    }

    updateFechas(e){
        console.log("Capturamos el evento de sidebar del cambio de fecha")
        console.log(e.detail);
        this.shadowRoot.querySelector('guardia-main').fechasFilter=e.detail;
    }

    guardiaStore(e){
        console.log("Capturamos evento de main con la nueva guardiaStore")
        console.log(e.detail)
        this.shadowRoot.querySelector('guardia-main').storeGuardia = e.detail;
        this.shadowRoot.querySelector('guardia-main').postGuardia=true;

    }
}

customElements.define('guardia-app', GuardiaApp)