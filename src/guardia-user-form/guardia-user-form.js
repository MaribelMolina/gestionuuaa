import { LitElement, html} from 'lit-element';


class GuardiaUserForm  extends LitElement {


    static get properties(){
        return {
            user: {type: Object}
        }
    }

    constructor(){
        super();
        this.resetFormData();

    }
    render(){ 
        return html`
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">      
        <div class="row" >
            <form>
                <div class="form-group">
                    <label>Código de usuario</label>
                    <input type="text" class="form-control" placeholder="Código de usuario" required
                    .value="${this.user.codUser}"
                    @input="${this.updateCodUser}"
                    >
                </div>
                <div class="form-group">
                    <label>Nombre Completo</label>
                    <input type="text" id="userFormName"/ class="form-control" placeholder="Nombre Completo" required
                    @input="${this.updateName}"
                    .value="${this.user.name}"
                    />
                </div>
                <div class="form-group">
                    <label>Teléfono</label>
                    <input type="tel" class="form-control" placeholder="número móvil" required
                    @input="${this.updateTelefono}"
                     .value="${this.user.telefono}">
                </div>
                <button class="btn btn-primary" @click="${this.goBack}"><strong>Atrás</strong></button>
                <button class="btn btn-success" @click="${this.storeUser}"><strong>Guardar</strong></button>     
            </form>
         </div>
        
        `;
    }

    updateCodUser(e){
        console.log("Actualizando la propiedad user con el valor" + e.target.value);
        this.user.codUser = e.target.value;
    }

    updateName(e){
        console.log("Actualizando la propiedad name con el valor" + e.target.value);
        this.user.name = e.target.value;
    }

    updateTelefono(e){
        console.log("Actualizando la propiedad teléfono con el valor" + e.target.value);
        this.user.telefono = e.target.value;
    }

    resetFormData() {
        this.user={};
        this.user.codUser= "";
        this.user.name= "";
        this.user.telefono= "";
    };

    goBack(e) {

        console.log("goBack");
        e.preventDefault();
        this.resetFormData();
        this.dispatchEvent(new CustomEvent("user-form-close",{}));


    }
    
    storeUser(e){
        console.log("StoreUser");
        e.preventDefault();

        console.log(this.user.codUser)
     
        this.dispatchEvent(new CustomEvent("user-form-store",{
            detail: {
                user: {
                    codUser: this.user.codUser,
                    name: this.user.name,
                    telefono: this.user.telefono,

                }
            }
        }));
        this.resetFormData();

    }

}

customElements.define('guardia-user-form', GuardiaUserForm)