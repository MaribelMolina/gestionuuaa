import { LitElement, html} from 'lit-element';

class GuardiaApi  extends LitElement {

    static get properties(){
        return {
            guardiasUUAA: {type: Array},
            storeGuardia: {type: Array},
            postGuardia: {type: Boolean}
        };

    }

    constructor(){
        super();
        this.postGuardia=false;

        this.guardiaUUAA= [
            {
                nameUUAA: "",
                fechaGuardia:"",
                user:{
                    codUser:"",
                    nameUser: "",
                    telUser: ""
                }
            }
        ];

        this.storeGuardia =[
            {
                uuaa: "",
                fecha: "",
                user:{
                    id:""
                }
            }
        ];

        this.getMangUUAA();
    }
  
    getMangUUAA(){

        
        console.log("getMangUUAA");
        console.log("Obteniendo datos de asignación de la UUAA")
             
        //Gestiona peticiones asícnronas y lo asignamos a un xhr
        
        let xhr= new XMLHttpRequest();

        if (this.postGuardia === true) {
            //método y url a la que tiene que llamar (se saca de la API). Abre el canal para preparar el envío
            xhr.open("POST","http://localhost:8081/apitechu/v1/uuaa");


            //Se va a llamar cuando se termine de cargar el envío de la petición asíncrona. Captura del evento
             xhr.onload = () => {
                 //Triple igual comprueba valor y tipo
                 if (xhr.status === 200){
                    console.log("Petición de POST completada correctamente");
                 }
                }
         //En el send se envía el body en el caso de que tenga body. El GET no tiene body pero el POST sí
            xhr.send(storeGuardia);
            this.postGuardia = false;
            

        }

        else {
        
            //Se va a llamar cuando se termine de cargar el envío de la petición asíncrona. Captura del evento
            xhr.onload = () => {
                //Triple igual comprueba valor y tipo
                if (xhr.status === 200){
                    console.log("Petición GET completada correctamente");

                    //Si hacemos console log de la respuesta vemos que es un json
                    console.log(xhr.responseText);
                    //al ser un json lo podemos parsear
                    let APIResponse = JSON.parse(xhr.responseText);
                    console.log(APIResponse);

                    this.guardiasUUAA=APIResponse.map(
                        (guardia) => {
                        return {
                                nameUUAA: guardia.uuaa,
                                fechaGuardia: guardia.fecha,
                                user:{
                                    codUser:guardia.users.id,
                                    nameUser: guardia.users.nombre,
                                    telUser: guardia.users.telefono
                                }
                            }
                        }
                            
                    );

                    console.log("Hemos asignado el resultado de APIResponse a guardiasUUAA")
                    console.log(this.guardiasUUAA)
                
                }
            }
        } 

        //método y url a la que tiene que llamar (se saca de la API). Abre el canal para preparar el envío
        xhr.open("GET","http://localhost:8081/apitechu/v1/uuaa");
        //En el send se envía el body en el caso de que tenga body. El GET no tiene body pero el POST sí
        xhr.send();
        

       /*this.guardiasUUAA =[
           {
                nameUUAA: "EKVL",
                fechaGuardia: '2020-11-01',
                user: {
                     nameUser: "Pepito Pérez",
                     codeUser: "u1021023",
                     telUser: "651800099"
                }
            },
            {
                nameUUAA: "EKAS",
                fechaGuardia: '2020-11-30',
                user: {
                     nameUser: "Juanito Navarro",
                     codeUser: "u199999",
                     telUser: "661904801"
                }
            },
            {
                nameUUAA: "TVPV",
                fechaGuardia: '2020-11-27',
                user: {
                     nameUser: "Luciano Pavaroti",
                     codeUser: "e050731",
                     telUser: "661203050"
                }
            },
            {
                nameUUAA: "TVNL",
                fechaGuardia: '2020-11-26',
                user: {
                     nameUser: "Juaquín Del Betis",
                     codeUser: "a102030",
                     telUser: "641203058"
                }
            }

         ];*/
           
         
        console.log("Fin de getMangUUAA");
        console.log(this.guardiasUUAA);
    }

    
    updated(changedProperties){

        console.log("Ha cambiado la propiedad")

        this.dispatchEvent(new CustomEvent("guardia-api",
                {
                detail: {
                    guardiasUUAA: this.guardiasUUAA
                }
            }
        ));
    }
}

customElements.define('guardia-api', GuardiaApi)