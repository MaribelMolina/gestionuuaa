import { LitElement, html} from 'lit-element';

class GuardiaSidebar  extends LitElement {


    static get properties(){
        return {
            fechaFin: {type: String},
            fechaInicio: {type: String}
        };
    }

    constructor(){
        super();  
        const today = new Date();
        const month = '' + (today.getMonth()+1).length == 1?"0"+today.getMonth() + 1: today.getMonth() + 1
        const day = '' + today.getDate().length == 1?"0"+today.getDate(): today.getDate()
        this.fechaFin=`${today.getFullYear()}-${month}-${day}`;
        this.fechaInicio=`${today.getFullYear()}-${month}-${day}`;

    }

    render(){ 
        return html`
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">      
           
            <aside>
                <section>
                    <div class="mt-5">
                    
                        <label>
                           <strong> Fecha Guardia <strong>
                        </label>
                        <p></p>
                        <label for="start">Inicio: </label>
                      
                        <input type="date" id="start" name="trip-start"
                                min="2018-01-01" max="2025-12-31"
                                value="${this.fechaInicio}"
                                @input="${this.updateFechaInicio}">
                        <p></p>

                        <label for="start">Fin: </label>
                        <input type="date" id="start" name="trip-start"
                                min="2018-01-01" max="2025-12-31"
                                value="${this.fechaFin}"
                                @input="${this.updateFechaFin}">
                        
                    </div>

                    <div class="mt-5">
                        <button class="w-100 btn btn-outline-success" style="font-size: 20px" @click="${this.newUser}"><strong>Añadir Usuario</strong></button>
                        <p></p>
                        <button class="w-100 btn btn-outline-primary" style="font-size: 20px" @click="${this.newUUAA}"><strong>Añadir UUAA</strong></button>
                        <p></p>
                        <button class="w-100 btn btn-outline-dark" style="font-size: 20px" @click="${this.newGuardia}"><strong>Añadir Guardia</strong></button>
                    </div>
                </section>
            </aside>
        `;
    }


    newUser(e){
        console.log("newUser en persona-sidebar");
        console.log("se va a crear un usuario nuevo");

        this.dispatchEvent( new CustomEvent(
            "new-user", {}));
    }

    newUUAA(e){
        console.log("newUUAA en persona-sidebar");
        console.log("se va a crear una nueva UUAA");

        this.dispatchEvent( new CustomEvent(
            "new-uuaa", {}));
    }

    newGuardia(e){
        console.log("newGuardia en persona-sidebar");
        console.log("se va a crear una nueva Guardia");

        this.dispatchEvent( new CustomEvent(
            "new-guardia", {}));
    }

    updateFechaInicio(e){
        console.log("la fecha de inicio es: ")
        console.log(e.target.value);
        this.fechaInicio=e.target.value;

    }

    updateFechaFin(e){
        console.log("la fecha fin es: ")
        console.log(e.target.value);

        this.fechaFin=e.target.value;
    }

    updated(changedProperties){
        console.log("updated");
        this.dispatchEvent(
            new CustomEvent("updated-fechas",{
            detail: {
                fechaInicio: this.fechaInicio,
                fechaFin: this.fechaFin
            }
        }
        ));
    }
}

customElements.define('guardia-sidebar', GuardiaSidebar)